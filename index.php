<?php
require 'vendor/autoload.php';
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Currency
{
    const CURRENCY = [
        'usd' => 'R01235',
        'cny' => 'R01375',
        'eur' => 'R01239'
    ];

    private $key = '8304tjslg';

    public $payload = [
        'iss' => 'localhost',
        'aud' => 'localhost',
        'username' => 'Ivan',
        'password' => 'Ivanov',
        'id' => '1'
    ];

    public function checkJWT()
    {
        $jwt = JWT::encode($this->payload, $this->key, 'HS256');
        $header = apache_request_headers();

        if($header['Authorization']){
            $header = $header['Authorization'];
            $decoded = JWT::decode($header, new Key($this->key, 'HS256'));
            return $decoded->id;
        }else{
            return false;
        }

    }

    public function getCurrency()
    {
        $id = $this->checkJWT();
        if($id){
            foreach (self::CURRENCY as $item){
                $url = 'https://cbr.ru/scripts/XML_dynamic.asp?date_req1='.$_GET['date_start'].'&date_req2='.$_GET['date_end'].'&VAL_NM_RQ='.$item;
                $xml = new SimpleXMLElement($url, 0, true);
                return json_encode($xml);
            }
        }

    }
}

$a = new Currency();
$a->getCurrency();
